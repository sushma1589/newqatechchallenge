# NewQATechChallenge

## Add your files

https://gitlab.com/sushma1589/newqatechchallenge.git

## Name
New QA Tech challenge is the project name

## Description
Add a vodafone sim plan and validate the same on cart page

## Installation
Cucumber library, maven project, JAVA and selenium

## Usage
Steps:

1. First a GET api call is being made and only plan names are retrieved
2. Later user navigates to URL page "https://www.vodafone.com.au/plans/sim-only"
3. On this page, first the value of plan is retrieved and the name of the plan is retrieved from the sim only page
4. Later user clicks on add to cart
5. In cart page, once user clicks see more, the text shown on plan is captured
6. Later the assertion is applied

Test execution steps:

1. Go to gitlab
2. Go to CI/CD 
3. CLick on run pipeline
3. You notice that build-> test stage is passed
4. However, job stage is in pending as it needs to be picked up by runner (Runner is actively runninG) but still job is still not being picked

## Support
sushma_mallesh@yahoo.com

## Project status
completed