package com.pages;

import static io.restassured.RestAssured.given;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import io.restassured.response.Response;

public class AddPlan {

	WebDriver driver;
	static String planName;
	static String sub;
	WebElement listItems;
	String amString;
	String plName;
	String pname;
	String cur;
	String totalVal;
	Properties pr = new Properties();
	private static final String BASE_URL = "https://api-prod.prod.cms.df.services.vodafone.com.au/plan/postpaid-simo?serviceType=New";
	public Response response;

	public AddPlan(WebDriver driver) {
		this.driver = driver;

	}

	public void filePath() throws IOException {
		String path = "./src/test/resources/config/config.properties";
		FileInputStream fis = new FileInputStream(path);
		pr.load(fis);
	}

	public static void parseJson() throws JSONException {
		Response response = given().get(BASE_URL);
		JSONObject jsonObj = new JSONObject(response.getBody().asString());
		System.out.println(jsonObj);
		String jArray = (response.jsonPath().getString("planListing"));
		System.out.println(jArray);
		JSONObject response1 = (JSONObject) jsonObj.get("planListing");
		JSONArray students = (JSONArray) response1.get("plans");
		for (int i = 0; i < students.length(); i++) {
			planName = (String) ((JSONObject) students.get(i)).get("planName");
			System.out.println(planName);
		}
	}

// @Given("I am on vodafone sim only page")
	public void navigateURL() throws IOException, InterruptedException, ParseException {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,600)");
	}

	public void getAmount() {
		//JavascriptExecutor js = (JavascriptExecutor) driver;
		//js.executeScript("window.scrollBy(0,800)");
		
		WebElement currency=driver.findElement(By.xpath
				("(//div[@id='plan-card-AU12273']//span[@class='Pricestyles__CurrencySymbolSmall-sc-1m965dg-9 kbvAPW'])[2]"));
		WebElement amount = driver.findElement(By.xpath(
				"(//div[@id='plan-card-AU12273']//h5[@class='Pricestyles__AmountSmall-sc-1m965dg-10 cKVHFB'])[2]"));

		WebElement name = driver.findElement(By.xpath(
				"(//div[@id='plan-card-AU12273']//div[@class='DataAllowancestyles__Label-sc-dqlyks-4 jsmMqk']/div)[2]"));
		
		cur=currency.getAttribute("innerText");
		System.out.println("currency is" +cur);

		amString = amount.getAttribute("innerText");
		System.out.println("amount is" + amString);
		
		totalVal=cur+amString;

		plName = name.getAttribute("innerHTML");
		pname = plName.substring(20);
		System.out.println("plan name selected is" + pname);

	}

	public void validatePlans() throws InterruptedException {
		Thread.sleep(4000);
		List<WebElement> listItems = driver
				.findElements(By.xpath("//div[@class='DataAllowancestyles__Label-sc-dqlyks-4 jsmMqk']/div"));
		for (int i = 0; i < listItems.size(); i++) {

			String valueSeen = listItems.get(i).getAttribute("innerHTML");
			sub = valueSeen.substring(20);

			System.out.print(sub + " ");
		}

		Assert.assertEquals(planName, sub);

	}

	// @When("i add a plan to cart")
	public void addPlanToCart() throws InterruptedException {
		Thread.sleep(3000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,800)");
		Thread.sleep(2000);
		WebElement cart = driver
				.findElement(By.xpath("(//div[@id='accordion-item-88']//button[contains(., 'Add to cart')])"));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].click();", cart);
	}

	// @Then("the price seen on cart")
	public void planIsAdded() throws InterruptedException {
		Thread.sleep(3000);
		WebElement seeMore = driver
				.findElement(By.xpath("//div[@class='StickyCartTitlestyles__CaretLabel-sc-1bvgkjv-5 cTQaCS']"));
		Actions gh = new Actions(driver);
		gh.moveToElement(seeMore).click().build().perform();
	}

	// @Then("the same plan is seen on cart page")
	public void sameProductAdded() {
		WebElement text = driver
				.findElement(By.xpath("(//text[@class='StickyCartItemstyles__ItemName-sc-bnfv9r-5 xVxKV']/div)[1]"));
		String f = text.getText();
		System.out.println(f);
		String d = totalVal+ " " + pname;
		System.out.println(d);
		Assert.assertEquals(f, d);

	}
}
