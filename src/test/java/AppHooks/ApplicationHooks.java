package AppHooks;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.pages.AddPlan;
import com.qa.factory.DriverFactory;
import com.qa.util.ConfigReader;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class ApplicationHooks {

	private DriverFactory driverFactory;
	private WebDriver driver;
	private ConfigReader configReader;

	Properties prop;

	@Before(order = 0)
	public void getProperty() {
		configReader = new ConfigReader();
		prop = configReader.init_prop();
	}

	@Before(order = 1)
	public void launchBrowser() {
		String browserName = prop.getProperty("browser");
		String URL = prop.getProperty("url");

		driverFactory = new DriverFactory();
		driver = driverFactory.init_driver(browserName);
		driver.get(URL);
		AddPlan.parseJson();

	}

	@After
	public void closeLogout(Scenario scenario) throws InterruptedException, IOException {

		Thread.sleep(3000);
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);

		String destination = System.getProperty("user.dir") + "\\target\\screenshotOfPlanAdded.png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);

		driver.quit();
	}

}
