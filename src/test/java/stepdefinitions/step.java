package stepdefinitions;

import java.io.IOException;

import org.json.simple.parser.ParseException;

import com.pages.AddPlan;
import com.qa.factory.DriverFactory;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class step {

	public AddPlan au = new AddPlan(DriverFactory.getDriver());


	@Given("I am on vodafone sim only page")
	public void i_am_on_vodafone_sim_only_page() throws InterruptedException, IOException, ParseException {

		au.navigateURL();
		au.getAmount();
		au.validatePlans();
		


	}

	@When("i add a plan to cart")
	public void i_add_a_plan_to_cart() throws InterruptedException {
		au.addPlanToCart();

	}

	@Then("the price seen on cart")
	public void the_price_seen_on_cart() throws InterruptedException {
		au.planIsAdded();

	}

	@Then("the same plan is seen on cart page")
	public void the_same_plan_is_seen_on_cart_page() throws InterruptedException, IOException
	{
		au.sameProductAdded();
	}

}
