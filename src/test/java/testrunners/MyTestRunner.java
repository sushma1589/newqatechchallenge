package testrunners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;


@RunWith(Cucumber.class)
@CucumberOptions(features = { "src/test/resources/AppFeatures/AddPlanToVodafoneSimOnlyPage.feature" }, glue = { "stepdefinitions",
		"AppHooks"},
plugin = { "pretty", "html:target/cucumber-reports.html"},
monochrome = true,publish=true

)

public class MyTestRunner {

}

